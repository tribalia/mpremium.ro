-- phpMyAdmin SQL Dump
-- version 4.0.10deb1ubuntu0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 10, 2019 at 07:00 PM
-- Server version: 5.5.61-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `acr_ro`
--

-- --------------------------------------------------------

--
-- Table structure for table `competitors`
--

CREATE TABLE IF NOT EXISTS `competitors` (
  `id_comp` int(11) NOT NULL AUTO_INCREMENT,
  `nume_comp` varchar(50) NOT NULL,
  `prenume_comp` varchar(50) NOT NULL,
  `data_nasterii` int(9) DEFAULT NULL,
  `cnp` int(13) DEFAULT NULL,
  PRIMARY KEY (`id_comp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `competitors`
--

INSERT INTO `competitors` (`id_comp`, `nume_comp`, `prenume_comp`, `data_nasterii`, `cnp`) VALUES
(6, 'ionescu', 'dan', 2006, 2147483647),
(7, 'ionescu', 'dan', 2006, 2147483647),
(8, 'Andrei', 'Lumy', 1998, 2147483647),
(9, 'Andrei', 'Lumy', 1998, 2147483647),
(10, 'Dumitru', 'Bogdan', 2019, 12123232);

-- --------------------------------------------------------

--
-- Table structure for table `participari`
--

CREATE TABLE IF NOT EXISTS `participari` (
  `id_comp` int(11) NOT NULL,
  `id_ral` int(11) NOT NULL,
  `data_participare` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participari`
--

INSERT INTO `participari` (`id_comp`, `id_ral`, `data_participare`) VALUES
(6, 2, '2019-02-05'),
(7, 2, '2019-02-05'),
(8, 1, '2019-02-03'),
(9, 1, '2019-02-03'),
(10, 3, '2019-02-12');

-- --------------------------------------------------------

--
-- Table structure for table `rally`
--

CREATE TABLE IF NOT EXISTS `rally` (
  `id_ral` int(11) NOT NULL AUTO_INCREMENT,
  `nume_ral` varchar(50) NOT NULL,
  `locatie_ral` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ral`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rally`
--

INSERT INTO `rally` (`id_ral`, `nume_ral`, `locatie_ral`) VALUES
(1, 'SWIMMING', 'Bucuresti'),
(2, 'DAKAR', 'Sibiu'),
(3, 'BEDUINI', 'Shara');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;